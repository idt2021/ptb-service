package proto

// 回傳給client的response
type Resp struct {
	Msg  string
	Data interface{}
}

// Data 是 map[string]interface{} 的簡寫
type Object map[string]interface{}
