package proto

import (
	"time"

	"gorm.io/gorm"
)

type History struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `json:"-" form:"-"`
}

// 一個用戶在一個idea上的代幣的紀錄
// 當存入代幣時，Amount為正數
// 當提出代幣時，Amount為負數
type Polls struct {
	History
	Pid    string `json:"pid,omitempty" form:"pid,omitempty" gorm:"primaryKey;not null;default:uuid_generate_v4()"`
	Iid    string `json:"iid" form:"iid,omitempty" binding:"required" gorm:"not null"`
	Uid    string `json:"uid" form:"uid"`
	Amount int    `json:"amount" form:"amount" binding:"required" gorm:"not null"`
}

// 一個用戶在一個idea中投給每個solution的紀錄(poll solution)
type Ps struct {
	History
	Psid   string `json:"psid,omitempty" form:"psid,omitempty" gorm:"primaryKey;not null;default:uuid_generate_v4()"`
	Sid    string `json:"sid,omitempty" binding:"required" form:"sid" gorm:"not null"`
	Iid    string `json:"iid" form:"iid" binding:"required" gorm:"not null"`
	Uid    string `gorm:"not null"`
	Amount int    `json:"amount" form:"amount" binding:"required" gorm:"not null"`
}

type Orders struct {
	History
	Oid    string `gorm:"primaryKey;not null;default:uuid_generate_v4()"`
	Uid    string
	Iid    string
	Price  int
	Side   string
	Origin int
	Remain int
}

type Preqs struct {
	History
	Prid string `gorm:"primaryKey"`
	Iid  string
	Link string `gorm:"unique"`
}

type Ratings struct {
	History
	Rid  string `gorm:"primaryKey;not null;default:uuid_generate_v4()"`
	Prid string
	Uid  string `gorm:"not null"`
	Love int    `gorm:"not null"`
}

type PrLoveRate struct {
	Prid     string  `json:"prid"`
	LoveRate float64 `json:"love_rate"`
	Link     string  `json:"link"`
}

type Bids struct {
	History
	Bid   string `gorm:"primaryKey;not null;default:uuid_generate_v4()"`
	Uid   string
	Iid   string
	Price int
}

type Bstatus struct {
	History
	Iid  string `gorm:"primaryKey"`
	Stat bool
}
