package proto

type Configuration struct {
	DbConfig     DbConfig     `json:"DbConfig"`
	ServerConfig ServerConfig `json:"ServerConfig"`
}

type DbConfig struct {
	Addr     string
	Port     string
	Username string
	Name     string // 資料庫的名稱
	Password string
}

type ServerConfig struct {
	Mode string // gin.Mode[release/debug/test]
	Addr string // empty string is allowed
	Port string
}
