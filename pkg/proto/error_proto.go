package proto

const ErrorExpectedPositive = "you should pass a positive number as parameter"
const ErrorInsufficientBalance = "insufficient balance"
const ErrorIncompleteRequest = "missing some required fields in request"
const ErrorIncompleteJWT = "missing some required fields in jwt"
const ErrorPermissionDenied = "permission denied"
const ErrorDatabaseError = "database error"

/* Trading */
const ErrorInvalidArg = "invalid argument"
const ErrorNotEnoughCoin = "user does not have enough coin"
const ErrorCoinAPI = "coin api return error"
const ErrorNotEnoughAmountToBought = "there is no enough selling amount to be bought"
const ErrorNotEnoughAmountToSell = "there is no enough selling amount to be sold"
const ErrorPrExists = "the pull request already exists"
const ErrorPrNotExists = "the pull request does not exist"

/* Bidding */
const ErrorDuplicateBidOpen = "the bidding of this idea is already opened"
const ErrorDuplicateBidClose = "the bidding of this idea is already closed"
const ErrorNotExistBid = "bidding of this idea is not opened yet"
const ErrorLessEqualThanLatestPrice = "price you enter should be greater than current price"
