package postgres

import (
	"errors"
	"idealTown/pkg/proto"

	"gorm.io/gorm"
)

// Description:
// 	對一個idea開啟競標(status設為1)
//
// Parameter:
// 	iid
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) OpenBid(iid string) error {
	var status bool = false
	bstatus := proto.Bstatus{
		Iid:  iid,
		Stat: true,
	}
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		err := m.Client.Model(&proto.Bstatus{}).
			Select("stat").
			Where("iid=?", iid).
			Take(&status).Error
		if (err != nil) && (err != gorm.ErrRecordNotFound) {
			return err
		}
		if status {
			return errors.New(proto.ErrorDuplicateBidOpen)
		}
		return m.Client.Create(&bstatus).Error
	})
	return err
}

// Description:
// 	對一個idea關閉競標(status設為0)
//
// Parameter:
// 	iid
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) CloseBid(iid string) error {
	var status bool
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		if err := m.Client.Model(&proto.Bstatus{}).
			Select("stat").
			Where("iid=?", iid).
			Take(&status).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				return errors.New(proto.ErrorNotExistBid)
			}
			return err
		}
		if !status {
			return errors.New(proto.ErrorDuplicateBidClose)
		}
		return m.Client.Model(&proto.Bstatus{}).
			Where("iid=?", iid).
			Update("stat", false).Error
	})
	return err
}

// Description:
// 	獲取idea的最新價格
//
// Parameter:
// 	iid
//
// Return:
// 	該idea的最新標價(int), nil
//
// Error:
// 	return (-1, error)
//
func (m *DBClient) GetLatestBidPrice(iid string) (int, error) {
	var status bool
	var price int
	tx := m.Client.Begin()
	if err := tx.Model(&proto.Bstatus{}).
		Select("stat").
		Where("iid=?", iid).
		Take(&status).Error; err != nil {
		tx.Rollback()
		if err == gorm.ErrRecordNotFound {
			return -1, errors.New(proto.ErrorNotExistBid)
		}
		return -1, err
	}
	if err := tx.Model(&proto.Bids{}).
		Select("COALESCE(MAX(price),0)").
		Where("iid=?", iid).
		Take(&price).Error; err != nil {
		tx.Rollback()
		return -1, err
	}
	if err := tx.Commit().Error; err != nil {
		return -1, err
	}
	return price, nil
}

// Description:
// 	對該idea喊價
//
// Parameter:
// 	iid, price(出價)
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) CreateBid(uid, iid string, price int) error {
	var status bool
	var p int //readin latest price
	if price <= 0 {
		return errors.New(proto.ErrorExpectedPositive)
	}
	tx := m.Client.Begin()
	if err := tx.Model(&proto.Bstatus{}).
		Select("stat").
		Where("iid=?", iid).
		Take(&status).Error; err != nil {
		tx.Rollback()
		if err == gorm.ErrRecordNotFound {
			return errors.New(proto.ErrorNotExistBid)
		}
		return err
	}
	if err := tx.Model(&proto.Bids{}).
		Select("COALESCE(MAX(price),0)").
		Where("iid=?", iid).
		Take(&p).Error; err != nil {
		tx.Rollback()
		return err
	}
	if price <= p {
		tx.Rollback()
		return errors.New(proto.ErrorLessEqualThanLatestPrice)
	}
	bid := proto.Bids{
		Uid:   uid,
		Iid:   iid,
		Price: price,
	}
	if err := tx.Create(&bid).Error; err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// Description:
// 	獲取該Idea的競標狀態(0為關閉, 1為開啟)
//
// Parameter:
// 	iid
//
// Return:
// 	bool,nil
//
// Error:
// 	return (false, error)
//
func (m *DBClient) GetBidStatus(iid string) (bool, error) {
	var status bool
	tx := m.Client.Begin()
	if err := tx.Model(&proto.Bstatus{}).
		Select("stat").
		Where("iid=?", iid).
		Take(&status).Error; err != nil {
		tx.Rollback()
		if err == gorm.ErrRecordNotFound {
			return false, errors.New(proto.ErrorNotExistBid)
		}
		return false, err
	}
	if err := tx.Commit().Error; err != nil {
		return false, err
	}
	return status, nil
}
