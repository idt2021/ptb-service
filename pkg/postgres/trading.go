package postgres

import (
	"errors"
	"gorm.io/gorm"
	"idealTown/pkg/proto"
)

// Description: 📌
// 	Insert an order into table "orders"
// Parameter:
//  client (db *gorm.DB), orders ([]proto.Orders), withTime (bool, whether insert custom created_at & updated_at)
//	If withTime is false, only 6 terms without created_at and updated_at will be inserted into the table.
//                             -> uid, iid, price, side, origin, remain
//  Otherwise, 8 columns will be set. -> plus 2 terms: created_at and updated_at
//  origin must >= 1; remain must >= 0; side must be "sell" or "buy"; price must >= 0;
//  Warning: If you set withTime true but did not fill time, the time will be the default time.
// Return:
// 	nil
// Error:
// 	err
func ClientInsertOrders(db *gorm.DB, orders []proto.Orders, withTime bool) error {
	/* Check arguments */
	for _, order := range orders {
		if !(order.Origin >= 1 && order.Remain >= 0 && (order.Side == "sell" || order.Side == "buy") && order.Price >= 0) {
			return errors.New(proto.ErrorInvalidArg)
		}
	}
	/* Batch insert */
	if withTime { // created_at and updated_at will be inserted into the table
		if err := db.Select("created_at", "updated_at", "Uid", "Iid", "Price", "Side", "Origin", "Remain").Create(&orders).Error; err != nil {
			return err
		}
	} else {
		if err := db.Select("Uid", "Iid", "Price", "Origin", "Remain", "Side").Create(&orders).Error; err != nil {
			return err
		}
	}
	return nil
}

// Description: 📌
// 	Insert a batch of orders into table "orders". Created_at & updated_at will be filled, too.
// Parameter:
// 	proto.Orders{created_at (time.Time), updated_at (time.Time), uid, iid, price, origin, remain, side}
//  More info about the arguments please refer to function ClientInsertOrders
// Return:
// 	nil
//
// Error:
// 	err
func (m *DBClient) InsertOrdersWithTime(orders []proto.Orders) error {
	if err := ClientInsertOrders(m.Client, orders, true); err != nil {
		return err
	}
	return nil
}

// Description: 📌
// 	Insert a batch of orders into table "orders" with no created_at & updated_at given.
//  Created_at and updated_at will be set automatically by gorm.
// Parameter:
// 	proto.Orders{uid, iid, price, origin, remain, side}
//  More info about the arguments please refer to function ClientInsertOrders
// Return:
// 	nil
// Error:
// 	err
func (m *DBClient) InsertOrders(orders []proto.Orders) error {
	if err := ClientInsertOrders(m.Client, orders, false); err != nil {
		return err
	}
	return nil
}

// 📌
// Description:
// 	Get all orders / all sell orders/ all buy orders. Sorted by created_at (ASC)
//
// Parameter:
//
// Return:
// 	[]proto.Orders, nil
//
// Error:
// 	nil, error
func (m *DBClient) GetAllOrdersByCreatedAt() ([]proto.Orders, error) {
	var orders []proto.Orders
	if err := m.Client.Select("*").Order("created_at ASC").Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

// 📌
type PriceAmount struct {
	Price  int `json:"price"`
	Amount int `json:"amount"`
}

// 📌
// Description:
// 	Get sell orders' amounts grouped by price.
//
// Parameter:
//  iid
//
// Return:
// 	[]PriceAmount, nil
//
// Error:
// 	nil, error
func (m *DBClient) ListSellPrices(iid string) ([]PriceAmount, error) {
	var sellPriceAmounts []PriceAmount
	if err := m.Client.Model(&proto.Orders{}).Select("price, sum(remain) as amount").Where("side = ? AND iid = ?", "sell", iid).Group("price").Find(&sellPriceAmounts).Error; err != nil {
		return nil, err
	}
	return sellPriceAmounts, nil
}

// 📌
type PriceOrigin struct {
	Price  int `json:"price"`
	Origin int `json:"origin"`
}

// 📌
// Description:
// 	Get the average buying price recently.
//
// Parameter:
//  n: recently n data of buying. n must >= 1
//
// Return:
// 	price (float64), nil
//
// Error:
// 	-1, error
func (m *DBClient) AvgRecentBuyPrice(iid string, n int) (float64, error) {
	if !(n >= 1) {
		return -1., errors.New(proto.ErrorInvalidArg)
	}
	var priceOrigins []PriceOrigin
	if err := m.Client.Model(&proto.Orders{}).Select("price, origin").Where("side = ? AND iid = ?", "buy", iid).Order("created_at desc").Find(&priceOrigins).Error; err != nil {
		return -1, err
	}
	remainN := n
	total := 0
	for _, priceOrigin := range priceOrigins {
		if remainN == 0 {
			break
		}
		if priceOrigin.Origin >= remainN {
			total += priceOrigin.Price * remainN
			remainN -= remainN
		} else {
			total += priceOrigin.Price * priceOrigin.Origin
			remainN -= priceOrigin.Origin
		}
	}
	avg := float64(total / (n - remainN))
	return avg, nil
}

// 📌
type OidPriceRemain struct {
	Oid    string `json:"oid"`
	Price  int    `json:"price"`
	Remain int    `json:"remain"`
}

// 📌
// Description:
// 	Buy in trading stage.
//
// Parameter:
//  n: number of amount to buy. n must >= 1
//  iid: idea id
//
// Return:
// 	nil
//
// Error:
// 	error
func (m *DBClient) TradeBuy(n int, uid string, iid string) error {
	/* Invalid arg */
	if !(n >= 1) {
		return errors.New(proto.ErrorInvalidArg)
	}

	/* Calculate total price and the info of orders to be bought */
	var oidPriceRemains []OidPriceRemain
	if err := m.Client.Model(&proto.Orders{}).Select("oid, price, remain").Where("side = ? AND iid = ?", "sell", iid).Order("price ASC, created_at ASC").Find(&oidPriceRemains).Error; err != nil {
		return err
	}
	remainN := n
	totalCoinNeeded := 0
	type toBuyOidPriceNum struct {
		Oid    string `json:"oid"`
		Price  int    `json:"price"`
		Remain int    `json:"remain"`
		Num    int    `json:"num"`
	}
	var toBuyOidPriceNums []toBuyOidPriceNum
	for _, oidPriceRemain := range oidPriceRemains {
		if remainN == 0 {
			break
		} else if oidPriceRemain.Remain == 0 {
			continue // Skip remain == 0
		} else if oidPriceRemain.Remain >= remainN {
			toBuyOidPriceNums = append(toBuyOidPriceNums, toBuyOidPriceNum{Oid: oidPriceRemain.Oid, Price: oidPriceRemain.Price, Remain: oidPriceRemain.Remain, Num: remainN})
			totalCoinNeeded += oidPriceRemain.Price * remainN
			remainN -= remainN
		} else {
			toBuyOidPriceNums = append(toBuyOidPriceNums, toBuyOidPriceNum{Oid: oidPriceRemain.Oid, Price: oidPriceRemain.Price, Remain: oidPriceRemain.Remain, Num: oidPriceRemain.Remain})
			totalCoinNeeded += oidPriceRemain.Price * oidPriceRemain.Remain
			remainN -= oidPriceRemain.Remain
		}
	}
	if remainN > 0 {
		return errors.New(proto.ErrorNotEnoughAmountToBought)
	}

	/* TODO: Check if user has enough money by calling API from COIN (Assume success) */
	// // Pass "totalCoinNeeded"
	//hasEnoughMoney := true
	//if !hasEnoughMoney {
	//	return errors.New(proto.ErrorNotEnoughCoin)
	//}

	/* Start transaction of func of buying */
	tx := m.Client.Begin()

	/* Add buyer's order and modify sellers' orders */
	for _, toBuyOidPriceNum := range toBuyOidPriceNums {
		// Add a buyer order
		if err := ClientInsertOrders(tx, []proto.Orders{{Uid: uid, Iid: iid, Price: toBuyOidPriceNum.Price, Side: "buy", Origin: toBuyOidPriceNum.Num, Remain: toBuyOidPriceNum.Num}}, false); err != nil {
			tx.Rollback()
			return err
		}
		// Modify sellers' orders
		if err := tx.Model(&proto.Orders{}).Where("oid = ?", toBuyOidPriceNum.Oid).Update("remain", toBuyOidPriceNum.Remain-toBuyOidPriceNum.Num).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	/* TODO: Subtract user's coin by calling API from COIN (Assume success) */
	//success := false
	//if !success{
	//	return errors.New(proto.ErrorCoinAPI)
	//}

	/* TODO: Add coin to those who successfully sell by calling API from COIN (Assume success) */
	//success := false
	//if !success{
	//	return errors.New(proto.ErrorCoinAPI)
	//}

	/* Finish transaction */
	if err := tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// 📌
// Description:
// 	Get a user's total available amount in trading stage of an idea.
//
// Parameter:
//  uid, iid
//
// Return:
// 	 total stock (float64), nil
//
// Error:
// 	-1, error
func (m *DBClient) TradeUserTotalStock(uid string, iid string) (float64, error) {
	var oidPriceTotals []struct {
		Price int `json:"price"`
		Total int `json:"total"`
	}
	if err := m.Client.Model(&proto.Orders{}).Select("price, SUM(remain) as total").Where("iid = ? AND uid = ? AND side = ?", iid, uid, "buy").Group("price").Find(&oidPriceTotals).Error; err != nil {
		return -1, err
	}
	sumPrice := 0
	sumAmount := 0
	for _, priceTotal := range oidPriceTotals {
		sumPrice += priceTotal.Price * priceTotal.Total
		sumAmount += priceTotal.Total
	}
	return float64(sumPrice) / float64(sumAmount), nil
}

// 📌
// Description:
// 	Sell stock
//
// Parameter:
//  uid, iid, price, amount
//  price must >= 0; amount must >= 1
//
// Return:
// 	nil
//
// Error:
// 	error
func (m *DBClient) TradeSell(price int, amount int, uid string, iid string) error {
	/* Check arguments */
	if !(price >= 0 && amount >= 1) {
		return errors.New(proto.ErrorInvalidArg)
	}

	/* Confirm if this user has this amount of stock to sell */
	var oidPriceRemains []struct {
		Oid    string `json:"oid"`
		Price  int    `json:"price"`
		Remain int    `json:"remain"`
	}
	if err := m.Client.Model(&proto.Orders{}).Select("oid, price, remain").Where("iid = ? AND uid = ? AND side = ?", iid, uid, "buy").Order("created_at ASC").Find(&oidPriceRemains).Error; err != nil {
		return err
	}
	sumRemains := 0
	for _, oidPriceRemain := range oidPriceRemains {
		sumRemains += oidPriceRemain.Remain
	}
	if sumRemains < amount {
		return errors.New(proto.ErrorNotEnoughAmountToSell)
	}

	/* Start transaction of func of buying */
	tx := m.Client.Begin()

	/* Subtract buy's remain */
	remainN := amount
	for _, oidPriceRemain := range oidPriceRemains {
		var curN int
		if remainN == 0 {
			break
		} else if oidPriceRemain.Remain >= remainN {
			curN = remainN
		} else {
			curN = oidPriceRemain.Remain
		}
		if err := tx.Model(&proto.Orders{}).Where("oid", oidPriceRemain.Oid).Update("remain", oidPriceRemain.Remain-curN).Error; err != nil {
			tx.Rollback()
			return err
		}
		remainN -= curN
	}

	/* Insert an order to sell */
	order := proto.Orders{Uid: uid, Iid: iid, Price: price, Side: "sell", Origin: amount, Remain: amount}
	if err := m.InsertOrders([]proto.Orders{order}); err != nil {
		return err
	}

	/* Finish transaction */
	if err := tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// 📌
// Description:
// 	Add a pull request
//
// Parameter:
//  uid, iid, link
//
// Return:
// 	nil
//
// Error:
// 	error
func (m *DBClient) AddPr(prid string, iid string, link string) error {
	/* Check if pr exists */
	var preqs []proto.Preqs
	if err := m.Client.Model(&proto.Preqs{}).Select("prid").Where("prid = ?", prid).Find(&preqs).Error; err != nil {
		return err
	}
	if len(preqs) != 0 {
		return errors.New(proto.ErrorPrExists)
	}
	/* Insert */
	newPr := proto.Preqs{Prid: prid, Iid: iid, Link: link}
	if err := m.Client.Select("prid", "iid", "link").Create(&newPr).Error; err != nil {
		return err
	}
	return nil
}

// 📌
// Description:
// 	Add a love value for a pull-request.
//
// Parameter:
//  prid, uid, love (must be -1 or 1)
//
// Return:
// 	nil
//
// Error:
// 	error
func (m *DBClient) AddPrLike(prid string, uid string, love int) error {
	/* Check arguments */
	if love != -1 && love != 1 {
		return errors.New(proto.ErrorInvalidArg)
	}
	/* Check if the pr exists */
	var preqs []proto.Preqs
	if err := m.Client.Where("prid = ?", prid).Find(&preqs).Error; err != nil {
		return err
	}
	if len(preqs) == 0 {
		return errors.New(proto.ErrorPrNotExists)
	}
	/* Check if the user has voted a love value to the pr of the idea */
	var ratings []proto.Ratings
	if err := m.Client.Where("prid = ? AND uid = ?", prid, uid).Find(&ratings).Error; err != nil {
		return err
	}
	if len(ratings) != 0 { // update
		if err := m.Client.Model(&proto.Ratings{}).Where("prid = ? AND uid = ?", prid, uid).Update("love", love).Error; err != nil {
			return err
		}
	} else { // insert
		rating := proto.Ratings{Prid: prid, Uid: uid, Love: love}
		if err := m.Client.Select("prid", "uid", "love").Create(&rating).Error; err != nil {
			return err
		}
	}
	return nil
}

// 📌
// Description:
// 	List an idea's all pull-requests' like average values.
//
// Parameter:
//  iid
//
// Return:
// 	[]proto.PrLoveRate, nil
//
// Error:
// 	nil, error
func (m *DBClient) ListPrLike(iid string) ([]proto.PrLoveRate, error) {
	var allPrLinkRatings []proto.PrLoveRate
	if err := m.Client.Model(&proto.Preqs{}).Select("preqs.prid as prid, AVG(love) as love_rate, link").Joins("join ratings on ratings.prid = preqs.prid").Where("preqs.iid = ?", iid).Group("preqs.prid").Order("iid ASC").Find(&allPrLinkRatings).Error; err != nil {
		return nil, err
	}
	return allPrLinkRatings, nil
}
