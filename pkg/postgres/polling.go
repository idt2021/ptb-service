package postgres

import (
	"errors"
	"idealTown/pkg/proto"

	"gorm.io/gorm"
)

// Description:
// 	將票存至某個idea
//
// Parameter:
// 	Polls{iid,uid,amount->這個要是正數}
//
// Return:
// 	填充後的proto.Polls,nil
//
// Error:
// 	return (nil,error)
//
func (m *DBClient) DepositPoll(poll proto.Polls) (*proto.Polls, error) {
	if poll.Amount <= 0 {
		return nil, errors.New(proto.ErrorExpectedPositive)
	}
	if err := m.Client.Create(&poll).Error; err != nil {
		return nil, err
	}
	return &poll, nil
}

// Description:
// 	將票從某個idea中扣除(扣掉的值要加到哪要再用別的函數決定)
//
// Parameter:
// 	Polls{iid,uid,amount->這個要是正數}
//
// Return:
// 	填充後的proto.Polls
//
// Error:
// 	return nil
func (m *DBClient) WithdrawFromPoll(poll proto.Polls) error {
	if poll.Amount <= 0 {
		return errors.New(proto.ErrorExpectedPositive)
	}
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		// 檢查餘額
		var remain int
		if err := tx.Model(&poll).Select("SUM(amount)").
			Where("iid=? AND uid=?", poll.Iid, poll.Uid).
			Scan(&remain).Error; err != nil {
			return err
		}
		if remain < poll.Amount {
			return errors.New(proto.ErrorInsufficientBalance)
		}
		// 加入負的餘額
		poll.Amount = -poll.Amount
		if err := tx.Create(&poll).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// Description:
// 	將錢錢轉移至某個solution
//
// Parameter:
// 	amount(int)->這個要是正數,iid,uid,sid(string)
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) TransferToSolution(amount int, iid, uid, sid string) error {
	if amount <= 0 {
		return errors.New(proto.ErrorExpectedPositive)
	}
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		poll := proto.Polls{
			Iid:    iid,
			Uid:    uid,
			Amount: -amount,
		}
		// 檢查餘額
		var remain int
		if err := tx.Model(&poll).Select("SUM(amount)").
			Where("iid=? AND uid=?", iid, uid).
			Scan(&remain).Error; err != nil {
			return err
		}

		if remain < amount {
			return errors.New(proto.ErrorInsufficientBalance)
		}
		// 建立新的poll
		if err := tx.Create(&poll).Error; err != nil {
			return err
		}
		// 建立新的Ps
		ps := proto.Ps{
			Sid:    sid,
			Iid:    iid,
			Uid:    uid,
			Amount: amount,
		}
		if err := tx.Create(&ps).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// Description:
// 	將錢錢從某個solution轉移到同一個idea之下的另一個solution
//
// Parameter:
// 	amount(int)->這個要是正數,iid,uid, from_sid, to_sid(string)
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) TransferS2S(amount int, uid, from_sid, to_sid string) error {
	if amount <= 0 {
		return errors.New(proto.ErrorExpectedPositive)
	}
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		// 查solution的iid
		ps := proto.Ps{}
		if err := tx.Model(&ps).
			Where("uid=? AND sid=?", uid, from_sid).
			Take(&ps).Error; err != nil {
			return err
		}
		// 檢查餘額是否夠多
		if ps.Amount < amount {
			return errors.New(proto.ErrorInsufficientBalance)
		}
		ps.Amount = -amount
		// 扣掉solution的amount
		if err := tx.Omit("psid").Create(&ps).Error; err != nil {
			return err
		}
		// 加進去另一個solution裡面
		ps.Sid = to_sid
		ps.Amount = amount
		if err := tx.Omit("psid").Create(&ps).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// Description:
// 	將錢錢從某個solution轉移回idea
//
// Parameter:
// 	amount(int)->這個要是正數,iid,uid,sid(string)
//
// Return:
// 	nil
//
// Error:
// 	return error
//
func (m *DBClient) WithdrawFromSolutionToIdea(amount int, uid, sid string) error {
	if amount <= 0 {
		return errors.New(proto.ErrorExpectedPositive)
	}
	err := m.Client.Transaction(func(tx *gorm.DB) error {
		// 查solution的iid
		ps := proto.Ps{}
		if err := tx.Model(&ps).
			Where("uid=? AND sid=?", uid, sid).
			Take(&ps).Error; err != nil {
			return err
		}
		// 檢查餘額是否夠多
		if ps.Amount < amount {
			return errors.New(proto.ErrorInsufficientBalance)
		}
		ps.Amount = -amount
		// 扣掉solution的amount
		if err := tx.Omit("psid").Create(&ps).Error; err != nil {
			return err
		}
		// 加回去Idea裡面
		poll := proto.Polls{
			Iid:    ps.Iid,
			Uid:    uid,
			Amount: amount,
		}
		if err := tx.Create(&poll).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

// Description:
// 	得到某個idea的poll的(總金額,solution總金額)
//
// Parameter:
// 	iid
//
// Return:
// 	idea池中的剩餘總金額
//
// Error:
// 	return -1
//
func (m *DBClient) GetPollStat(iid string) (int, int, error) {
	var idea, solution int
	if err := m.Client.Model(&proto.Polls{}).
		Select("SUM(amount) as amount").
		Where("iid=?", iid).
		Scan(&idea).Error; err != nil {
		return -1, -1, err
	}

	if err := m.Client.Model(&proto.Ps{}).
		Select("SUM(amount) as amount").
		Where("iid=?", iid).
		Scan(&solution).Error; err != nil {
		return -1, -1, err
	}
	return idea, solution, nil
}

// Description:
// 	得到某個idea池中的剩餘總金額(不包含solution)
//
// Parameter:
// 	iid
//
// Return:
// 	idea池中的剩餘總金額,nil
//
// Error:
// 	return -1,error
func (m *DBClient) GetPoll(iid string) (int, error) {
	var total int
	if err := m.Client.Model(&proto.Polls{}).
		Select("SUM(amount)").
		Where("iid=?", iid).Scan(&total).Error; err != nil {
		return -1, err
	}
	return total, nil
}

// Description:
// 	得到指定idea池中每個Solution對應的總金額
//
// Parameter:
// 	iid
//
// Return:
// 	map[solution_id] = int,nil
//
// Error:
// 	return -1,error
//
func (m *DBClient) GetPs(iid string) (map[string]int, error) {
	rows, err := m.Client.Model(&proto.Ps{}).
		Select("sid, SUM(amount) as amount").
		Where("iid=?", iid).
		Group("sid").Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	result := make(map[string]int)
	for rows.Next() {
		var key string
		var val int
		rows.Scan(&key, &val)
		result[key] = val
	}
	return result, nil
}

// Description:
// 	得到user在IdeaId池的剩餘金額
//
// Parameter:
// 	iid, uid (string)
//
// Return:
//	投入的Amount
//
// Error:
//	return nil
//
func (m *DBClient) GetPollByUser(iid, uid string) (int, error) {
	var amount int
	if err := m.Client.Model(&proto.Polls{}).
		Select("SUM(amount) as amount").
		Where("iid=?", iid).Scan(&amount).Error; err != nil {
		return 0, nil
	}
	return amount, nil
}

// Description:
// 		得到user在指定的idea中每個Solution對應的金額
//
// Parameter:
// 		`iid`, `uid` (string)
//
// Return:
// 		map[sid]=amount (每個solution分別投入的Amount)
//
// Error:
// 		return nil
//
func (m *DBClient) GetPsByUser(iid, uid string) (map[string]int, error) {
	rows, err := m.Client.Model(&proto.Ps{}).
		Select("sid, SUM(amount) as amount").
		Where("iid=? And uid=?", iid, uid).
		Group("sid").Rows()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	results := map[string]int{}
	for rows.Next() {
		var key string
		var val int
		rows.Scan(&key, &val)
		results[key] = val
	}
	return results, nil
}
