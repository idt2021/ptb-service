package postgres

import (
	"fmt"
	"idealTown/pkg/proto"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// 操作資料庫的代理介面
type DBClient struct {
	Client *gorm.DB
}

// 連線到資料庫，可以使用DBClient存取
func Connect(config *proto.DbConfig) *DBClient {
	// config := getDbConfig(configPath)
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable TimeZone=Asia/Taipei",
		config.Addr,
		config.Port,
		config.Username,
		config.Name,
		config.Password,
	)
	// 新的postgre gorm不用 defer close
	// 在修改、增加資料庫TABLE表頭時，忽略外來鍵檢查
	client, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		panic(err)
	}
	return &DBClient{Client: client}
}

// 測試時初始化資料庫使用(限定資料庫名稱為testdb)
func (m *DBClient) Initialize() {
	if m.Client.Migrator().CurrentDatabase() == "testdb" {
		m.Client.Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")
		m.Client.Exec("CREATE EXTENSION IF NOT EXISTS \"pgcrypto\";")
		// 刪除舊的表
		if err := m.Client.Migrator().DropTable(
			"ratings",
			"preqs",
			"orders",
			"polls",
			"ps",
			"bids",
			"bstatuses",
		); err != nil {
			log.Fatal(err.Error())
		}
		// 更換為新的表
		if err := m.Client.AutoMigrate(
			&proto.Ratings{},
			&proto.Preqs{},
			&proto.Orders{},
			&proto.Polls{},
			&proto.Ps{},
			&proto.Bids{},
			&proto.Bstatus{},
		); err != nil {
			log.Fatal(err.Error())
		}
		// 插入測試用的資料

	} else {
		log.Fatal("在非測試的資料庫中使用Initialize()會導致非預期的資料遺失，請勿使用!")
	}
}
