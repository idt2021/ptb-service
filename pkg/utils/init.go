package utils

import (
	"idealTown/pkg/proto"
	"log"
	"os"

	"github.com/joho/godotenv"
)

/* Get config */
func GetConfig(filepath string) *proto.Configuration {
	if err := godotenv.Load(filepath); err != nil {
		log.Println("無法讀取.env")
	}
	return &proto.Configuration{
		DbConfig: proto.DbConfig{
			Addr:     os.Getenv("POSTGRES_HOST"),
			Port:     os.Getenv("POSTGRES_PORT"),
			Name:     os.Getenv("POSTGRES_DB"),
			Username: os.Getenv("POSTGRES_USER"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		},
		ServerConfig: proto.ServerConfig{
			Mode: os.Getenv("ServerMode"),
			Port: os.Getenv("ServerPort"),
		},
	}
}
