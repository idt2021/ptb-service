package api

import (
	pg "idealTown/pkg/postgres"
	"idealTown/pkg/proto"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func PollStat(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查iid是不是有值
		iid := c.Param("iid")
		if iid == "" {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		// 取得統計
		idea, solution, err := client.GetPollStat(iid)
		if err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		c.JSON(http.StatusOK, proto.Resp{
			Data: proto.Object{
				"idea":     idea,
				"solution": solution,
			},
		})
	}
}

func PollStats(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查iid是不是有值
		iid := c.Param("iid")
		if iid == "" {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		// 取得idea的總額
		idea, err := client.GetPoll(iid)
		if err != nil {
			c.JSON(http.StatusNotFound, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		// 取得每個solution的總額
		solutions, err := client.GetPs(iid)
		if err != nil {
			c.JSON(http.StatusNotFound, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		c.JSON(http.StatusOK, proto.Resp{
			Data: proto.Object{
				"idea":     idea,
				"solution": solutions,
			},
		})
	}
}

func PollUserStat(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 檢查iid是不是有值
		iid := c.Param("iid")
		if iid == "" {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		idea, err := client.GetPollByUser(iid, uid)
		if err != nil {
			c.JSON(http.StatusNotFound, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		solutions, err := client.GetPsByUser(iid, uid)
		if err != nil {
			c.JSON(http.StatusNotFound, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		c.JSON(http.StatusOK, proto.Resp{
			Data: proto.Object{
				"idea":     idea,
				"solution": solutions,
			},
		})
	}
}

func AddPoll(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 資料綁定Poll
		var poll proto.Polls
		if err := c.ShouldBind(&poll); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		poll.Uid = uid
		NewPoll, err := client.DepositPoll(poll)
		if err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		c.JSON(http.StatusOK, proto.Resp{
			Data: proto.Object{
				"amount": NewPoll.Amount,
				"pid":    NewPoll.Pid,
				"iid":    NewPoll.Iid,
			},
		})
	}
}

func AddPs(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 資料綁定Ps
		var ps proto.Ps
		if err := c.ShouldBind(&ps); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		// 將錢錢從idea中轉入solution
		if err := client.TransferToSolution(ps.Amount, ps.Iid, uid, ps.Sid); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
	}
}

func WithdrawFromIdeaToWallet(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 資料綁定Poll
		var poll proto.Polls
		if err := c.ShouldBind(&poll); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		poll.Uid = uid
		// 將錢從idea中移除
		if err := client.WithdrawFromPoll(poll); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
		// 添加到錢包
		log.Fatal("Coin Service Do Not Complete")
	}
}

func WithdrawFromSolutionToIdea(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 資料綁定Ps
		var ps proto.Ps
		if err := c.ShouldBind(&ps); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		// 將錢從solution移至idea
		if err := client.WithdrawFromSolutionToIdea(ps.Amount, uid, ps.Sid); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
	}
}

func TransferBetweenSolutions(client *pg.DBClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		// 檢查JWT token裡面的userId
		val, exist := c.Get("uid")
		if !exist {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteJWT,
			})
			return
		}
		uid := val.(string)
		// 檢查form
		amount, _ := strconv.Atoi(c.PostForm("amount"))
		from := c.PostForm("from")
		to := c.PostForm("to")
		if amount <= 0 || from == "" || to == "" {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorIncompleteRequest,
			})
			return
		}
		// 轉移
		if err := client.TransferS2S(amount, uid, from, to); err != nil {
			c.JSON(http.StatusBadRequest, proto.Resp{
				Msg: proto.ErrorDatabaseError,
			})
			return
		}
	}
}
