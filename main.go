package main

import (
	"fmt"
	proto "idealTown/pkg/proto"
	utils "idealTown/pkg/utils"

	"github.com/gin-contrib/cors"

	"github.com/gin-gonic/gin"
)

/* GLOBAL VARIABLE */
var config *proto.Configuration

func main() {
	// Load Configuration
	config = utils.GetConfig(".env")
	gin.SetMode(config.ServerConfig.Mode)

	// Initialize WebServer
	server := gin.Default()
	server.Use(cors.Default()) // Allow CORS

	// APIs
	api := server.Group("/api")
	{
		poll := api.Group("/poll")
		{
			poll.GET("/stat/:iid")
			poll.GET("/idea/:iid")
			poll.GET("/idea/:iid/user/:uid")
			poll.POST("/")
			poll.DELETE("/")
		}
		trading := api.Group("/trading")
		{
			trading.GET("/")
			trading.GET("/avg")
			trading.POST("/")
			trading.PUT("/")
			trading.GET("/love/:iid")
		}
	}

	// Server Start
	ip := fmt.Sprintf("%s:%s",
		config.ServerConfig.Addr,
		config.ServerConfig.Port)
	server.Run(ip)
}
