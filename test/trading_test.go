package test

import (
	"idealTown/pkg/postgres"
	"idealTown/pkg/proto"
	"sort"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

// Test: InsertOrders, InsertOrdersWithTime, GetAllOrdersByCreatedAt, ClientInsertOrders
func TestInsertOrders(t *testing.T) {
	/* Test invalid arguments */
	orders := []proto.Orders{
		{Uid: "1", Iid: "1", Price: -1, Origin: 2, Remain: 1, Side: "sell"},           // invalid price
		{Uid: "23", Iid: "1", Price: 100, Origin: 0, Remain: 0, Side: "buy"},          // invalid origin
		{Uid: "20", Iid: "2", Price: 100, Origin: 5, Remain: -1, Side: "sell"},        // invalid remain
		{Uid: "20", Iid: "2", Price: 100, Origin: 5, Remain: 2, Side: "good morning"}, // invalid side
	}
	for _, order := range orders {
		client.Initialize()
		err := client.InsertOrders([]proto.Orders{order})
		assert.Error(t, err, proto.ErrorInvalidArg) // Check if it raises correct error
	}

	/* Test InsertOrders (without setting created_at and updated_at) */
	client.Initialize()
	orders = []proto.Orders{
		{Uid: "1", Iid: "1", Price: 100, Origin: 2, Remain: 1, Side: "sell"},
		{Uid: "23", Iid: "1", Price: 100, Origin: 3, Remain: 0, Side: "buy"},
	}
	err := client.InsertOrders(orders)
	assert.Nil(t, err)
	record, err := client.GetAllOrdersByCreatedAt()
	assert.Nil(t, err)
	assert.Equal(t, len(orders), len(record))
	sort.SliceStable(record, func(i, j int) bool { // Sort it because we didn't assign created_at and updated_at
		return record[i].Price+record[i].Origin < record[j].Price+record[j].Origin
	})
	if len(orders) == len(record) {
		for i := 0; i < len(orders); i++ { // Values of CreatedAt, UpdatedAt and Oid can be different.
			assert.Equal(t, gorm.DeletedAt{}, record[i].DeletedAt)
			assert.Equal(t, orders[i].Uid, record[i].Uid)
			assert.Equal(t, orders[i].Iid, record[i].Iid)
			assert.Equal(t, orders[i].Price, record[i].Price)
			assert.Equal(t, orders[i].Side, record[i].Side)
			assert.Equal(t, orders[i].Origin, record[i].Origin)
			assert.Equal(t, orders[i].Remain, record[i].Remain)
		}
	}

	/* Test InsertOrdersWithTime & GetAllOrdersByCreatedAt */
	client.Initialize()
	t1 := time.Now()
	orders = []proto.Orders{
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 2), UpdatedAt: t1.Add(time.Minute * 8)}, Uid: "1", Iid: "1", Price: 100, Origin: 2, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 8), UpdatedAt: t1.Add(time.Minute * 16)}, Uid: "23", Iid: "1", Price: 100, Origin: 3, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 4), UpdatedAt: t1.Add(time.Minute * 4)}, Uid: "20", Iid: "2", Price: 100, Origin: 5, Remain: 5, Side: "sell"},
	}
	err = client.InsertOrdersWithTime(orders)
	assert.Nil(t, err)
	record, err = client.GetAllOrdersByCreatedAt()
	assert.Nil(t, err)
	assert.Equal(t, len(orders), len(record))

	sort.SliceStable(orders, func(i, j int) bool {
		// To check if GetAllOrdersByCreatedAt return ordered by CreatedAT, sort the answers
		return orders[i].CreatedAt.Before(orders[j].CreatedAt)
	})

	timeWarningString := "由於系統插入時間會有小數點位數的誤差，所以在此測試中允許了一分鐘內的誤差（應該很夠了吧）；" +
		"換句話說，如果這個還有錯，極大機率是code有錯，系統誤差造成錯誤的機率趨近於零。"

	if len(orders) == len(record) {
		for i := 0; i < len(orders); i++ { // Values of history and Oid can be different.
			assert.True(t, orders[i].CreatedAt.Sub(record[i].CreatedAt) < time.Minute ||
				orders[i].CreatedAt.Sub(record[i].CreatedAt) > -1*time.Minute, timeWarningString)
			assert.True(t, orders[i].UpdatedAt.Sub(record[i].UpdatedAt) < time.Minute ||
				orders[i].UpdatedAt.Sub(record[i].UpdatedAt) > -1*time.Minute, timeWarningString)
			assert.Equal(t, gorm.DeletedAt{}, record[i].DeletedAt)
			assert.Equal(t, orders[i].Uid, record[i].Uid)
			assert.Equal(t, orders[i].Iid, record[i].Iid)
			assert.Equal(t, orders[i].Price, record[i].Price)
			assert.Equal(t, orders[i].Side, record[i].Side)
			assert.Equal(t, orders[i].Origin, record[i].Origin)
			assert.Equal(t, orders[i].Remain, record[i].Remain)
		}
	}
}

// Test: ListSellPrices
func TestListSellPrices(t *testing.T) {
	/* Reset Database */
	client.Initialize()
	t1 := time.Now()
	orders := []proto.Orders{
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 1), UpdatedAt: t1.Add(time.Minute * 1)}, Uid: "1", Iid: "1", Price: 100, Origin: 2, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 2), UpdatedAt: t1.Add(time.Minute * 2)}, Uid: "2", Iid: "1", Price: 100, Origin: 3, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 3), UpdatedAt: t1.Add(time.Minute * 3)}, Uid: "2", Iid: "2", Price: 100, Origin: 5, Remain: 5, Side: "sell"}, // not related to iid 1
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 4), UpdatedAt: t1.Add(time.Minute * 4)}, Uid: "3", Iid: "1", Price: 500, Origin: 3, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 5), UpdatedAt: t1.Add(time.Minute * 5)}, Uid: "1", Iid: "1", Price: 500, Origin: 2, Remain: 2, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 6), UpdatedAt: t1.Add(time.Minute * 6)}, Uid: "2", Iid: "1", Price: 500, Origin: 1, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 7), UpdatedAt: t1.Add(time.Minute * 7)}, Uid: "2", Iid: "1", Price: 500, Origin: 1, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 8), UpdatedAt: t1.Add(time.Minute * 8)}, Uid: "2", Iid: "1", Price: 800, Origin: 6, Remain: 3, Side: "sell"},
	}
	_ = client.InsertOrdersWithTime(orders)
	/* Start test */
	result, err := client.ListSellPrices("1")
	assert.Nil(t, err)
	assert.Equal(t, result, []postgres.PriceAmount{
		{Price: 100, Amount: 4},
		{Price: 500, Amount: 6},
		{Price: 800, Amount: 3},
	})
}

// Test: AvgRecentBuyPrice
func TestAvgRecentBuyPrice(t *testing.T) {
	/* Test valid arguments */
	client.Initialize()
	t1 := time.Now()
	orders := []proto.Orders{
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 1), UpdatedAt: t1.Add(time.Minute * 1)}, Uid: "1", Iid: "1", Price: 100, Origin: 2, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 2), UpdatedAt: t1.Add(time.Minute * 2)}, Uid: "2", Iid: "1", Price: 100, Origin: 9, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 3), UpdatedAt: t1.Add(time.Minute * 3)}, Uid: "2", Iid: "2", Price: 100, Origin: 5, Remain: 0, Side: "buy"}, // not related to iid 1
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 4), UpdatedAt: t1.Add(time.Minute * 4)}, Uid: "2", Iid: "1", Price: 100, Origin: 5, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 5), UpdatedAt: t1.Add(time.Minute * 5)}, Uid: "3", Iid: "1", Price: 500, Origin: 3, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 6), UpdatedAt: t1.Add(time.Minute * 6)}, Uid: "1", Iid: "1", Price: 500, Origin: 2, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 7), UpdatedAt: t1.Add(time.Minute * 7)}, Uid: "2", Iid: "1", Price: 700, Origin: 1, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 8), UpdatedAt: t1.Add(time.Minute * 8)}, Uid: "2", Iid: "1", Price: 700, Origin: 6, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 9), UpdatedAt: t1.Add(time.Minute * 9)}, Uid: "2", Iid: "1", Price: 700, Origin: 1, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 10), UpdatedAt: t1.Add(time.Minute * 10)}, Uid: "2", Iid: "1", Price: 800, Origin: 6, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 11), UpdatedAt: t1.Add(time.Minute * 11)}, Uid: "2", Iid: "1", Price: 800, Origin: 6, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 12), UpdatedAt: t1.Add(time.Minute * 12)}, Uid: "2", Iid: "1", Price: 1000, Origin: 6, Remain: 0, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 13), UpdatedAt: t1.Add(time.Minute * 13)}, Uid: "2", Iid: "1", Price: 800, Origin: 6, Remain: 0, Side: "buy"},
	}
	_ = client.InsertOrdersWithTime(orders)
	/* Start test */
	// Tested buy data:
	// 100 * 9  oldest
	// 100 * 5
	// 500 * 2
	// 700 * 6
	// 800 * 6
	// 1000 * 6
	// 800 * 6  newest
	testN := []int{5, 10, 20, 24, 40, 53}
	testAns := []float64{800., 880., 850., 825., 555., 555.}
	for i := 0; i < len(testN); i++ {
		result, err := client.AvgRecentBuyPrice("1", testN[i])
		assert.Nil(t, err)
		assert.Equal(t, testAns[i], result)
	}
	/* Test invalid arguments */
	result, err := client.AvgRecentBuyPrice("1", 0)
	expected := float64(-1)
	assert.Equal(t, expected, result)
	assert.Error(t, err, proto.ErrorInvalidArg)
}

// Test: TradeBuy, TradeUserTotalStock, TradeSell
// Assume COIN API always return successfully (e.g. user always has enough coin to pay for an order)
func TestTradeBuySell(t *testing.T) {
	/* Init Database */
	client.Initialize()
	t1 := time.Now()
	orders := []proto.Orders{
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 1), UpdatedAt: t1.Add(time.Minute * 1)}, Uid: "1", Iid: "1", Price: 100, Origin: 2, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 2), UpdatedAt: t1.Add(time.Minute * 2)}, Uid: "3", Iid: "1", Price: 500, Origin: 3, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 3), UpdatedAt: t1.Add(time.Minute * 3)}, Uid: "3", Iid: "2", Price: 500, Origin: 3, Remain: 3, Side: "sell"}, // not be considered due to the iid
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 4), UpdatedAt: t1.Add(time.Minute * 4)}, Uid: "2", Iid: "1", Price: 700, Origin: 13, Remain: 1, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 5), UpdatedAt: t1.Add(time.Minute * 5)}, Uid: "2", Iid: "1", Price: 700, Origin: 7, Remain: 3, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 6), UpdatedAt: t1.Add(time.Minute * 6)}, Uid: "2", Iid: "1", Price: 700, Origin: 7, Remain: 3, Side: "buy"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 7), UpdatedAt: t1.Add(time.Minute * 7)}, Uid: "2", Iid: "1", Price: 300, Origin: 9, Remain: 6, Side: "sell"},
		{History: proto.History{CreatedAt: t1.Add(time.Minute * 8), UpdatedAt: t1.Add(time.Minute * 8)}, Uid: "2", Iid: "1", Price: 300, Origin: 11, Remain: 4, Side: "sell"},
	}
	_ = client.InsertOrdersWithTime(orders)

	/* Start test TradeBy */
	// Tested sell data:
	// 100 * 1  oldest
	// 500 * 3
	// 700 * 3
	// 300 * 6
	// 300 * 4  newest
	// testNs := []int{1, 5, 6}

	// Buy some
	testNs := []int{1, 5, 6}
	for i := 0; i < len(testNs); i++ {
		err := client.TradeBuy(testNs[i], "999", "1")
		assert.Nil(t, err)
	}
	/* Test TradeBuy: test if all sell orders are modified correctly */
	record, _ := client.GetAllOrdersByCreatedAt()
	type filteredRecord struct {
		Uid    string `json:"uid"`
		Iid    string `json:"iid"`
		Price  int    `json:"price"`
		Side   string `json:"side"`
		Origin int    `json:"origin"`
		Remain int    `json:"remain"`
	}
	var filteredRecords []filteredRecord
	for _, row := range record {
		if row.Iid == "1" && row.Side == "sell" {
			filteredRecords = append(filteredRecords, filteredRecord{row.Uid, row.Iid, row.Price, row.Side, row.Origin, row.Remain})
		}
	}
	buyOrderAns := []filteredRecord{
		{"1", "1", 100, "sell", 2, 0},
		{"3", "1", 500, "sell", 3, 2},
		{"2", "1", 700, "sell", 13, 1},
		{"2", "1", 700, "sell", 7, 3},
		{"2", "1", 300, "sell", 9, 0},
		{"2", "1", 300, "sell", 11, 0},
	}
	assert.Equal(t, buyOrderAns, filteredRecords)

	/* Test TradeBuy: test if all buy orders are inserted proper */
	record, _ = client.GetAllOrdersByCreatedAt()
	filteredRecords = []filteredRecord{}
	for _, row := range record {
		if row.Uid == "999" {
			filteredRecords = append(filteredRecords, filteredRecord{row.Uid, row.Iid, row.Price, row.Side, row.Origin, row.Remain})
		}
	}
	buyOrderAns = []filteredRecord{
		{"999", "1", 100, "buy", 1, 1}, // 1 (time order)
		{"999", "1", 300, "buy", 5, 5}, // 2
		{"999", "1", 300, "buy", 1, 1}, // 3
		{"999", "1", 300, "buy", 4, 4}, // 4
		{"999", "1", 500, "buy", 1, 1}, // 5
	}
	for _, item := range buyOrderAns {
		assert.Contains(t, filteredRecords, item)
	}

	/* Test: not enough to be bought */
	err := client.TradeBuy(999, "555", "1")
	assert.Error(t, err, proto.ErrorNotEnoughAmountToBought)

	/* Test TradeUserTotalStock: sum of a user's num of all bought */
	stock, err := client.TradeUserTotalStock("999", "1")
	expectedStock := float64(300)
	assert.Nil(t, err)
	assert.Equal(t, expectedStock, stock)

	/* Test TradeSell: invalid arguments */
	err = client.TradeSell(-100, 1, "1", "1")
	assert.Error(t, err, proto.ErrorInvalidArg)
	err = client.TradeSell(100, 0, "1", "1")
	assert.Error(t, err, proto.ErrorInvalidArg)

	/* Test TradeSell: ErrorNotEnoughAmountToSell */
	err = client.TradeSell(999, 99999, "999", "1")
	assert.Error(t, err, proto.ErrorNotEnoughAmountToSell)

	/* Test TradeSell: whether add a sell order correctly */
	err = client.TradeSell(999, 9, "999", "1")
	assert.Nil(t, err)
	filteredRecords = []filteredRecord{}
	record, _ = client.GetAllOrdersByCreatedAt()
	for _, row := range record {
		if row.Uid == "999" && row.Side == "sell" {
			filteredRecords = append(filteredRecords, filteredRecord{row.Uid, row.Iid, row.Price, row.Side, row.Origin, row.Remain})
		}
	}
	sellOrderAns := []filteredRecord{{"999", "1", 999, "sell", 9, 9}}
	assert.Equal(t, filteredRecords, sellOrderAns)

	/* Test TradeSell: whether subtract buy orders' remains correctly */
	record, _ = client.GetAllOrdersByCreatedAt()
	filteredRecords = []filteredRecord{}
	for _, row := range record {
		if row.Uid == "999" && row.Side == "buy" {
			filteredRecords = append(filteredRecords, filteredRecord{row.Uid, row.Iid, row.Price, row.Side, row.Origin, row.Remain})
		}
	}
	buyOrderAns = []filteredRecord{
		{"999", "1", 100, "buy", 1, 0}, // 1 (time order)
		{"999", "1", 300, "buy", 5, 0}, // 2
		{"999", "1", 300, "buy", 1, 0}, // 3
		{"999", "1", 300, "buy", 4, 2}, // 4
		{"999", "1", 500, "buy", 1, 1}, // 5
	}
	assert.Equal(t, filteredRecords, buyOrderAns)
}

// Test: AddPr, AddPrLike, ListPrLike
func TestPrLike(t *testing.T) {
	client.Initialize()
	/* Test AddPr: duplicate pr adding */
	err := client.AddPr("1", "1", "https://1")
	assert.Nil(t, err)
	err = client.AddPr("1", "1", "https://1")
	assert.NotNil(t, err)

	/* Test AddPrLike: invalid arguments */
	err = client.AddPrLike("1", "345", 12345)
	assert.Error(t, err, proto.ErrorInvalidArg)

	/* Test AddPrLike: non-existing pr */
	err = client.AddPrLike("Hello", "345", -1)
	assert.Error(t, err, proto.ErrorPrNotExists)

	/* Integrally Test */
	// Insert init data
	client.Initialize()
	preqs := [][]string{
		{"1", "1", "https://1"},
		{"2", "1", "https://2"},
		{"3", "1", "https://3"},
		{"4", "2", "https://4"}}
	ratings := []struct {
		Prid string `json:"prid"`
		Uid  string `json:"uid"`
		Love int    `json:"love"`
	}{
		{"1", "1", 1},
		{"1", "2", 1},
		{"1", "3", -1},
		{"1", "4", -1},
		{"2", "11", 1},
		{"2", "12", 1},
		{"3", "1", -1},
		{"3", "102", -1}}
	for _, preq := range preqs {
		err := client.AddPr(preq[0], preq[1], preq[2])
		assert.Nil(t, err)
	}
	for _, rating := range ratings {
		err := client.AddPrLike(rating.Prid, rating.Uid, rating.Love)
		assert.Nil(t, err)
	}
	// Start test
	result, err := client.ListPrLike("1")
	sort.SliceStable(result, func(i, j int) bool {
		iNum, err := strconv.Atoi(result[i].Prid)
		assert.Nil(t, err)
		jNum, err := strconv.Atoi(result[j].Prid)
		assert.Nil(t, err)
		return iNum < jNum
	})
	assert.Nil(t, err)
	assert.Len(t, result, 3)
	assert.Equal(t, result, []proto.PrLoveRate{
		{Prid: "1", LoveRate: 0, Link: "https://1"},
		{Prid: "2", LoveRate: 1, Link: "https://2"},
		{Prid: "3", LoveRate: -1, Link: "https://3"},
	})
}
