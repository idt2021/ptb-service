package test

import (
	"errors"
	"idealTown/pkg/proto"
	"testing"

	"github.com/stretchr/testify/assert"
)

// 測試 Open, Close, GetBidStat
func TestOpenCloseGetBidStat(t *testing.T) {
	/* Reset Database */
	client.Initialize()
	/* Dummy data */
	const iid1 = "idea1"
	const iid2 = "idea2"
	/* Start test */
	err := client.OpenBid(iid1)
	assert.Nil(t, err)
	err = client.OpenBid(iid1)
	assert.Equal(t, errors.New(proto.ErrorDuplicateBidOpen), err)
	status, err := client.GetBidStatus(iid1)
	assert.Nil(t, err)
	assert.Equal(t, true, status)
	status, err = client.GetBidStatus(iid2)
	assert.Equal(t, errors.New(proto.ErrorNotExistBid), err)
	assert.Equal(t, false, status)
	err = client.CloseBid(iid1)
	assert.Nil(t, err)
	err = client.CloseBid(iid1)
	assert.Equal(t, errors.New(proto.ErrorDuplicateBidClose), err)
	status, err = client.GetBidStatus(iid1)
	assert.Nil(t, err)
	assert.Equal(t, false, status)
	err = client.CloseBid(iid2)
	assert.Equal(t, errors.New(proto.ErrorNotExistBid), err)
}

// 測試 GetLatestBidPrice, CreateBid
func TestGetLatestPriceCreateBid(t *testing.T) {
	/* Reset Database */
	client.Initialize()
	/* Dummy data */
	const iid1 = "idea1"
	const iid2 = "idea2"
	const uid1 = "user1"
	const uid2 = "user2"
	/* Start test */
	client.OpenBid(iid1)
	price, err := client.GetLatestBidPrice(iid1)
	assert.Nil(t, err)
	assert.Equal(t, 0, price)
	price, err = client.GetLatestBidPrice(iid2)
	assert.Equal(t, errors.New(proto.ErrorNotExistBid), err)
	assert.Equal(t, -1, price)
	err = client.CreateBid(uid1, iid2, 25)
	assert.Equal(t, errors.New(proto.ErrorNotExistBid), err)
	err = client.CreateBid(uid1, iid1, 25)
	assert.Nil(t, err)
	err = client.CreateBid(uid1, iid1, -2)
	assert.Equal(t, errors.New(proto.ErrorExpectedPositive), err)
	price, err = client.GetLatestBidPrice(iid1)
	assert.Nil(t, err)
	assert.Equal(t, 25, price)
	err = client.CreateBid(uid2, iid1, 15)
	assert.Equal(t, errors.New(proto.ErrorLessEqualThanLatestPrice), err)
	err = client.CreateBid(uid2, iid1, 25)
	assert.Equal(t, errors.New(proto.ErrorLessEqualThanLatestPrice), err)
	err = client.CreateBid(uid1, iid1, 30)
	assert.Nil(t, err)
	price, err = client.GetLatestBidPrice(iid1)
	assert.Nil(t, err)
	assert.Equal(t, 30, price)
	err = client.CreateBid(uid2, iid1, 50)
	assert.Nil(t, err)
	price, err = client.GetLatestBidPrice(iid1)
	assert.Nil(t, err)
	assert.Equal(t, 50, price)
}
