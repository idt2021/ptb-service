package test

import (
	"idealTown/pkg/postgres"
	"idealTown/pkg/proto"
	"idealTown/pkg/utils"
	"testing"

	"github.com/stretchr/testify/assert"
)

var config = utils.GetConfig("../.env")
var client = postgres.Connect(&config.DbConfig)

// 測試 Deposit, TransferToSolution, WithdrawFromSolution, GetPollStat, GetPoll, GetPs
func TestDepositTransferGetPollStatGetPs(t *testing.T) {
	/* Reset Database */
	client.Initialize()
	/* Dummy data */
	const uid1 = "1"
	const iid1 = "1"
	/* Start test */
	poll1, _ := client.DepositPoll(proto.Polls{
		Iid:    iid1,
		Uid:    uid1,
		Amount: 300,
	})
	assert.NotNil(t, poll1)
	res := client.TransferToSolution(100, iid1, uid1, "a")
	assert.Nil(t, res)
	res = client.TransferToSolution(30, iid1, uid1, "b")
	assert.Nil(t, res)
	res = client.TransferToSolution(-100000000, iid1, uid1, "a")
	assert.NotNil(t, res)
	/*
		Expected Result:
		IdeaID=1 amount:170
			| - solutionId : "a"  amount:100
			| - solutionId : "b"  amount:30
	*/
	total, solution_total, _ := client.GetPollStat(iid1)
	assert.Equal(t, 170, total)
	assert.Equal(t, 130, solution_total)

	total, _ = client.GetPoll(iid1)
	assert.Equal(t, 170, total)

	sols, _ := client.GetPs(iid1)
	assert.Equal(t, 100, sols["a"])
	sols, _ = client.GetPs(iid1)
	assert.Equal(t, 30, sols["b"])

	err := client.WithdrawFromSolutionToIdea(100, uid1, "a")
	assert.Nil(t, err)
	sols, _ = client.GetPs(iid1)
	assert.Equal(t, 0, sols["a"])

	err = client.WithdrawFromSolutionToIdea(10, uid1, "b")
	assert.Nil(t, err)
	sols, _ = client.GetPs(iid1)
	assert.Equal(t, 20, sols["b"])
	val, _ := client.GetPollByUser(iid1, uid1)
	assert.Equal(t, 280, val)

	err = client.WithdrawFromSolutionToIdea(500, uid1, "b")
	assert.Equal(t, proto.ErrorInsufficientBalance, err.Error())
	sols, _ = client.GetPs(iid1)
	assert.Equal(t, 20, sols["b"])
	/*
		Recent:
		IdeaID=1 amount:280
			| - solutionId : "a"  amount:0
			| - solutionId : "b"  amount:20
	*/
	err = client.TransferS2S(20, uid1, "b", "a")
	assert.Nil(t, err)
	sols, _ = client.GetPs(iid1)
	assert.Equal(t, 20, sols["a"])
	assert.Equal(t, 0, sols["b"])
}

func TestWithdrawFromPoll(t *testing.T) {
	/* Reset Database */
	client.Initialize()
	/* Dummy data */
	const uid1 = "1"
	const iid1 = "1"
	/* Start test */
	poll1, _ := client.DepositPoll(proto.Polls{
		Iid:    iid1,
		Uid:    uid1,
		Amount: 300,
	})
	assert.NotNil(t, poll1)
	err := client.WithdrawFromPoll(proto.Polls{
		Iid:    iid1,
		Uid:    uid1,
		Amount: 299,
	})
	assert.Nil(t, err)
	/*
		Expected Result:
		IdeaID=1 amount:1
	*/
	total, _ := client.GetPoll(iid1)
	assert.Equal(t, 1, total)
}

func TestGetPollAndPsByUser(t *testing.T) {
	/* Reset Database */
	client.Initialize()

	client.DepositPoll(proto.Polls{
		Iid:    "idea1",
		Uid:    "alan",
		Amount: 100,
	})
	client.TransferToSolution(50, "idea1", "alan", "sol1")
	client.TransferToSolution(40, "idea1", "alan", "sol2")
	/*
		Expected Result:
		IdeaID:"idea1" amount:10
			| - "sol1": 50
			| - "sol2": 40
	*/
	poll_test_1, err := client.GetPsByUser("idea1", "alan")
	assert.Nil(t, err)
	assert.Equal(t, 50, poll_test_1["sol1"])
	assert.Equal(t, 40, poll_test_1["sol2"])
	amount, err := client.GetPollByUser("idea1", "alan")
	assert.Nil(t, err)
	assert.Equal(t, 10, amount)
}
